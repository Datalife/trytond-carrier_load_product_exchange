===============================
Stock Move Done2Cancel Scenario
===============================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences
    >>> from trytond.modules.stock_unit_load.tests.tools import create_unit_load


Install carrier_load_product_exchange::

    >>> config = activate_modules(['carrier_load_product_exchange', 
    ...     'carrier_load_done2running', 'stock_party_warehouse',
    ...     'stock_move_done2cancel'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')


Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']


Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()


Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.customer_taxes.append(tax)
    >>> account_category.save()


Get warehouse and dock::

    >>> Location = Model.get('stock.location')
    >>> wh, = Location.find([('type', '=', 'warehouse')])
    >>> dock = wh.docks.new()
    >>> dock.name = 'Dock 1'
    >>> dock.code = 'D1'
    >>> wh.save()


Create parties::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer_loc = Location(type='customer', name='Customer 1')
    >>> customer_loc.save()
    >>> customer.customer_location = customer_loc
    >>> customer.create_warehouse = True
    >>> customer.save()
    >>> party_carrier = Party(name='Carrier 1')
    >>> party_carrier.save()


Create products::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> Template = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category')
    >>> category.save()
    >>> template = Template()
    >>> template.name = 'Plastic Case 30x30'
    >>> template.categories.append(category)
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.account_category = account_category
    >>> template.save()
    >>> product, = template.products
    >>> product.cost_price = Decimal('5')
    >>> product.save()

    >>> transport_template = Template(name='Transport')
    >>> transport_template.type = 'service'
    >>> transport_template.list_price = Decimal(500)
    >>> transport_template.default_uom = unit
    >>> transport_template.purchasable = True
    >>> transport_template.purchase_uom = unit
    >>> transport_template.account_category = account_category
    >>> transport_template.save()


Configure default location::

    >>> storage, = Location.find([('code', '=', 'STO')])
    >>> cat_loc = storage.categories.new()
    >>> cat_loc.category = category
    >>> storage.save()


Configure carrier configuration::

    >>> Configuration = Model.get('carrier.configuration')
    >>> configuration = Configuration(1)
    >>> configuration.product_exchange_category = category
    >>> configuration.exchange_quantity = 'empty'
    >>> configuration.save()


Create carrier::

    >>> Carrier = Model.get('carrier')
    >>> carrier = Carrier()
    >>> carrier.party = party_carrier
    >>> carrier.carrier_product = transport_template.products[0]
    >>> carrier.save()
    >>> carrier_loc = Location(type='supplier', name='Carrier 1')
    >>> carrier_loc.save()
    >>> carrier_party = carrier.party
    >>> carrier_party.supplier_location = carrier_loc
    >>> carrier_party.save()
    >>> carrier_product = carrier.carrier_product.template
    >>> carrier_product.save()


Create unit load::

    >>> ul = create_unit_load(config=config, product=product,
    ...     default_values={'start_date': datetime.datetime.now() - relativedelta(days=1)})


Create sale::

    >>> Sale = Model.get('sale.sale')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.invoice_method = 'order'
    >>> sale.warehouse = wh
    >>> sale_line = sale.lines.new()
    >>> sale_line.product = product
    >>> sale_line.quantity = 70.0
    >>> sale_line.ul_quantity = 1.0
    >>> sale.click('quote')
    >>> sale.click('confirm')


Start loading::

    >>> Loadorder = Model.get('carrier.load.order')
    >>> start_load = Wizard('carrier.load.create_wizard', [sale])
    >>> start_load.form.dock = sale.warehouse.docks[0]
    >>> start_load.form.carrier = carrier
    >>> start_load.form.vehicle_number = 'MX4459'
    >>> start_load.form.lines[0].ul_quantity = 1.0
    >>> start_load.execute('pre_load_')
    >>> load_order = Loadorder(sale.loads[0].id)
    >>> start_load = Wizard('carrier.load_uls', [load_order])
    >>> start_load.form.ul_code = ul.code
    >>> start_load.execute('load_')
    >>> exchange_move, = load_order.exchange_moves
    >>> exchange_move.quantity
    0.0
    >>> load_order.click('do') # doctest: +ELLIPSIS
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: The quantity of exchange movement "..." of Load order "..." cannot be zero. - 


Change configuration exchange quantity::

    >>> configuration.exchange_quantity = 'empty_warn'
    >>> configuration.save()


Do order::

    >>> load_order.click('do') # doctest: +ELLIPSIS
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserWarning: The quantity of exchange movement "..." of Load order "..." cannot be zero. - 
    >>> Model.get('res.user.warning')(user=config.user,
    ...     name='exchange_move_quantity_%s_%s' % (
    ...     exchange_move.id, load_order.id)).save()
    >>> load_order.click('do')
    >>> load_order.state
    'done'
    >>> exchange_move.reload()
    >>> exchange_move.state
    'done'


Modify exchange move::

    >>> modify_move = Wizard('carrier.load.order.modify_exchange_move', [load_order])
    >>> len(modify_move.form.exchange_moves)
    1
    >>> move, = modify_move.form.exchange_moves
    >>> move.state
    'draft'
    >>> move.quantity = 10.0
    >>> modify_move.execute('modify')
    >>> load_order.reload()
    >>> exchange_move.reload()
    >>> exchange_move.quantity
    10.0
    >>> exchange_move.state
    'done'
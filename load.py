# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.pyson import Eval, If
from trytond.transaction import Transaction
from trytond.exceptions import UserError
from trytond.i18n import gettext
try:
    from trytond.modules.stock_party_warehouse.model import ReturnableMoveMixin
except ImportError:
    ReturnableMoveMixin = object


class LoadOrder(metaclass=PoolMeta):
    __name__ = 'carrier.load.order'

    exchange_moves = fields.One2Many('stock.move',
        'origin', 'Exchange moves', states={
            'invisible': ~Eval('exchange'),
            'readonly': ~Eval('exchange') | Eval('state').in_(
                ['done', 'cancelled'])
        }, domain=[
            ('product.categories', '=', Eval('product_exchange_category', -1)),
            If(
                (Eval('state') != 'done'),
                ('from_location', '=', Eval('exchange_location')),
                ()
            )
        ],
        depends=['product_exchange_category', 'exchange_location',
            'exchange', 'state'])
    exchange = fields.Boolean('Exchange',
        states={'readonly': Eval('state').in_(['cancelled', 'done'])},
        depends=['state'])
    product_exchange_category = fields.Function(fields.Many2One(
        'product.category', 'Product Exchange Category'),
        'get_product_exchange_category')
    exchange_location = fields.Function(
        fields.Many2One('stock.location', 'Exchange location', states={
            'invisible': ~Eval('exchange')
            }, depends=['exchange']),
        'get_exchange_location')

    @staticmethod
    def default_exchange():
        return True

    def get_product_exchange_category(self, name):
        Configuration = Pool().get('carrier.configuration')
        conf = Configuration(1)
        if conf.product_exchange_category:
            return conf.product_exchange_category.id

    @classmethod
    def do(cls, records):
        pool = Pool()
        Move = pool.get('stock.move')

        for record in records:
            for exchange_move in record.exchange_moves:
                if not exchange_move.quantity:
                    record.get_exchange_quantity_error(exchange_move)

        super().do(records)
        for record in records:
            Move.assign(record.exchange_moves)
            Move.do(record.exchange_moves)

    def find_move_by_product(self, product):
        for move in self.exchange_moves:
            if move.product == product:
                return move

    def get_exchange_location(self, name=None):
        location = self._get_outgoing_moves_location(None)
        return location.id if location else None

    def get_exchange_quantity_error(self, exchange_move):
        raise UserError(gettext(
            'carrier_load_product_exchange.'
            'msg_carrier_load_order_exchange_moves_quantity',
            move=exchange_move.rec_name,
            order=self.rec_name))

    def _get_outgoing_moves_location(self, move):
        exchange_category = self.product_exchange_category
        if (self.load.purchasable
                and exchange_category
                and (
                (
                    move
                    and exchange_category in move.product.categories
                ) or not move)):
            return self.load.carrier.party.supplier_location
        return super()._get_outgoing_moves_location(move)

    @classmethod
    def _get_exchange_allowed_types(cls):
        return ('out', )

    def update_exchange_moves_from_ul(self, ul, add=True):
        pool = Pool()
        Configuration = pool.get('carrier.configuration')
        Move = pool.get('stock.move')
        Uom = pool.get('product.uom')

        conf = Configuration(1)
        if (self.type not in self._get_exchange_allowed_types()
                or not self.exchange
                or not conf.product_exchange_category):
            return

        moves_category = [move for move in ul.last_moves
            if conf.product_exchange_category in move.product.categories]

        for move_category in moves_category:
            from_location = self._get_outgoing_moves_location(move_category)
            exchange_move = self.find_move_by_product(move_category.product)
            qty = move_category.quantity * (1 if add else -1)

            if exchange_move:
                if exchange_move.state == 'done':
                    with Transaction().set_context(check_origin=False,
                            check_shipment=False):
                        Move.cancel([exchange_move])
                        Move.draft([exchange_move])
                if (exchange_move.state == 'done'
                        or conf.exchange_quantity == 'propose'):
                    exchange_move.quantity += Uom.compute_qty(
                        move_category.uom,
                        qty, exchange_move.uom)
                if (exchange_move.quantity <= 0
                        and (
                            conf.exchange_quantity == 'propose'
                            or (conf.exchange_quantity != 'propose'
                                and exchange_move.state == 'done'
                            )
                        )):
                    Move.delete([exchange_move])
                    return
            elif qty > 0 or conf.exchange_quantity != 'propose':
                if conf.exchange_quantity != 'propose':
                    qty = 0

                default_location = (conf.product_exchange_category.
                    get_default_location(warehouse=self.warehouse)
                    or self.warehouse.storage_location)

                exchange_move = Move(
                    product=move_category.product,
                    quantity=qty,
                    uom=move_category.uom,
                    unit_price=move_category.product.cost_price,
                    to_location=default_location,
                    from_location=from_location,
                    origin=self)
            if exchange_move:
                exchange_move.save()
            if self.state == 'done':
                Move.do([exchange_move])

    def add_ul(self, unit_loads, origin_restrict=None,
            origin_restrict_warn=True, force=False):
        res = super().add_ul(unit_loads,
            origin_restrict=origin_restrict,
            origin_restrict_warn=origin_restrict_warn, force=force)
        if res:
            # return failed uls
            return res
        for ul in unit_loads:
            self.update_exchange_moves_from_ul(ul)

    def _get_exchange_quantities(self):
        values = {}
        exchange_category = self.product_exchange_category
        if not exchange_category:
            return values
        for move in self.exchange_moves:
            values.setdefault(move.product, [0, 0])
            values[move.product][0] += move.internal_quantity
        for move in self.outgoing_moves:
            if exchange_category in move.product.categories:
                values.setdefault(move.product, [0, 0])
                values[move.product][1] += move.internal_quantity
        return values


class TransportReportMixin(object):

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(TransportReportMixin, cls).get_context(
            records, header, data)
        report_context['exchange_values'] = lambda order: \
            cls.get_exchange_data(order)
        return report_context

    @classmethod
    def get_exchange_data(cls, order):
        if not order:
            return {}
        return order._get_exchange_quantities()


class CMR(TransportReportMixin, metaclass=PoolMeta):
    __name__ = 'carrier.load.order.cmr'


class RoadTransportNote(TransportReportMixin, metaclass=PoolMeta):
    __name__ = 'carrier.load.order.road_note'


class LoadOrder2(metaclass=PoolMeta):
    __name__ = 'carrier.load.order'

    @classmethod
    def run(cls, records):
        Move = Pool().get('stock.move')
        to_run = [r for r in records if r.state == 'done']

        super().run(records)

        if to_run:
            moves = [m for o in to_run for m in o.exchange_moves]
            with Transaction().set_context(check_origin=False,
                    check_shipment=False):
                Move.cancel(moves)
                Move.draft(moves)


class CMRTemplate(metaclass=PoolMeta):
    __name__ = 'carrier.load.cmr.template'

    @classmethod
    def _get_section_defaults(cls):
        defaults = super()._get_section_defaults()
        defaults['6-9'] += ("\n"
            "${'\\n'.join(['%s: Loaded: %s  Returned: %s' % "
            "(k.name, lang.format('%d', v[1]), lang.format('%d', v[0])) "
            "for k, v in record._get_exchange_quantities().items()])}"
        )
        return defaults


class LoadOrderReturnable(ReturnableMoveMixin, metaclass=PoolMeta):
    __name__ = 'carrier.load.order'
    _to_return_moves = 'exchange_moves'

    def get_returnables_locations(self):
        warehouse = self.get_party_warehouse_used(
            **self._get_party_warehouse_pattern())
        return (
            warehouse and warehouse.storage_location,
            self.party and self.party.customer_location,
        )

    def _get_returnable_move_values(self, move):
        from_loc, to_loc = self.get_returnables_locations()
        return (
                ('origin', str(move)),
            ) + super()._get_returnable_move_values(move)

    def get_returnables_moves(self, name):
        pool = Pool()
        Move = pool.get('stock.move')

        from_loc, to_loc = self.get_returnables_locations()
        moves = Move.search([
            ('from_location', '=', from_loc),
            ('to_location', '=', to_loc),
            ('origin', 'like', 'stock.move,%'),
            ('origin.origin', '=', 'carrier.load.order,%s' % self.id,
                'stock.move')
        ])
        return list(map(int, moves))

    @classmethod
    def cancel(cls, records):
        cls.delete_returnables_moves(records)
        super().cancel(records)

    @classmethod
    def do(cls, records):
        super().do(records)
        cls.create_returnables_moves(records)


class LoadOrderReturnable2(metaclass=PoolMeta):
    __name__ = 'carrier.load.order'

    @classmethod
    def run(cls, records):
        to_run = [r for r in records if r.state == 'done']

        super().run(records)
        if to_run:
            cls.delete_returnables_moves(to_run)

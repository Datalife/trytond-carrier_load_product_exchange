# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields


class Configuration(metaclass=PoolMeta):
    __name__ = 'carrier.configuration'

    product_exchange_category = fields.Many2One(
        'product.category', 'Product Exchange Category', required=True)
    exchange_quantity = fields.Selection([
        ('propose', 'Propose'),
        ('empty', 'Empty')], 'Exchange quantity')

    @classmethod
    def default_exchange_quantity(cls):
        return 'propose'


class Configuration2(metaclass=PoolMeta):
    __name__ = 'carrier.configuration'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.exchange_quantity.selection.append(('empty_warn', 'Empty warning'))
